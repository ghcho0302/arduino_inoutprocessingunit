/*
 * Parameter.h
 *
 *  Created on: 2018. 11. 16.
 *      Author: Gyuhyun Cho
 */
//Main_ErrorDetectionMode
//Test change 
#define ACTIVE 1
#define INACTIVE 0

#define IN_PROCESSING_MODE 1
#define OUT_PROCESSING_MODE 2
#define CHECK_PROCESSING_MODE 3
#define ERROR_PROCESSING_MODE 0

#define WRITE_SUCCESSFUL 1
#define DUPLICATE 2
#define NOT_IN_DB 3

//Wifi Parameter
#define WIFI_RX 6
#define WIFI_TX 2

#define WIFI_MAX_TRIAL 2
#define WIFI_TIMEOUT 4000

#define WIFI_CONNECTED 1
#define WIFI_DISCONNECTED 0

#define WIFI_AP_NAME "iptime"
#define WIFI_AP_PASSWORD "o533540869"
#define WIFI_SERVER_IP "192.168.0.14"
#define WIFI_SERVER_PORT "27015"

#define IN_MODE 0
#define OUT_MODE 1
#define CHECK_MODE 2


//LCD Parameter
#define LCD_SS_PIN 6
#define LCD_DC_PIN 8

#define LCD_LINE1_X 0
#define LCD_LINE1_Y 20
#define LCD_LINE2_X 0
#define LCD_LINE2_Y 40
#define LCD_LINE3_X 0
#define LCD_LINE3_Y 60

//SPI communication
#define SPI_RST_PIN 9

#define RF_SS_PIN 10

//Button Parameter
#define BUTTON1_NO 7
#define BUTTON2_NO 4
#define BUTTON3_NO 5

//
#define BEEP_VOLUME 10

#define BEEP_SOUND_MODE_INP1 1
#define BEEP_SOUND_MODE_INP2 2
#define BEEP_SOUND_MODE_INP3 3
#define BEEP_SOUND_MODE_INP4 4
#define BEEP_SOUND_MODE_INP5 5
#define BEEP_SOUND_MODE_INP6 6

#define BUZ_1_PIN 3

//
#define RFID_WAITING_TIME 20

