
#include <Arduino.h>
#include <U8glib.h>
#include "Library\Parameter.h"
#include <MFRC522.h>
#include <SoftwareSerial.h>
#include <AltSoftSerial.h>
#include <string.h>
#include <arduino.h>

void Main_Mode_Setup();
void Main_Mode_Init();
void Main_Routine ( char Button_Input, char Main_Status ) ;
char Main_Mode( char Button, char LCD, char Wifi, char RFID );
void Main_InProcessingMode();
void Main_OutProcessingMode();
void Main_CheckProcessingMode();
void Main_ErrorProcessingMode();
void Main_WaitingMode();
void Main_Mode_Run();

bool BUTTON_Setup();
char BUTTON_Init();
char BUTTON_Routine();
int BUTTON_GetInput(int Input);
bool BUZZER_Setup();
void BUZZER_Beep(int BuzzerPin, int Volume, int MelodyType);
void BUZZER_Test();

void LCD_Setup();
char LCD_Init();
void LCD_Print_L1(char* StringStream);
void LCD_Print_L2(char* StringStream, char* stringStream2);
void LCD_PrintLine1(U8GLIB_SH1106_128X64_2X dev, uint8_t posX1, uint8_t posY1, char* stringStream1);
void LCD_PrintLine2(U8GLIB_SH1106_128X64_2X dev, uint8_t posX1, uint8_t posY1, char* stringStream1, uint8_t posX2, uint8_t posY2, char* stringStream2);

void RFID_Setup();
char RFID_Init();
bool RFID_Read();
char* RFID_Receive(unsigned int WaitingTimeMS);

void Wifi_Setup();
bool Wifi_Init();
char Wifi_ConnectionCheck();
bool Wifi_ConnectInit(String APName, String APPassword, String SVRIP, String SVRPort);
bool Wifi_ConnectAP(String APName, String APPassword);
bool Wifi_ConnectTCP(String SVRIP, String SVRPort);
int Wifi_sendPacket(char* Input);
void Wifi_receivePacket(char* string);
char* Wifi_MakePacket(char* InputUUID, int Mode);

int Wifi_sendPacket_Test(char* Input);

//Device
U8GLIB_SH1106_128X64_2X LCD_0(LCD_SS_PIN, LCD_DC_PIN, SPI_RST_PIN); // HW SPI
SoftwareSerial esp8266Serial = SoftwareSerial (6,2);
//AltSoftSerial esp8266Serial(6,2);
MFRC522 rfid(RF_SS_PIN, SPI_RST_PIN); // Instance of the class

//Global Variable

byte nuidPICC[4];
char CardUid[8];
char* RFID_id;
char RFID_idValue[9];
char Wifi_Packet[13];

void setup()
{
	Main_Mode_Setup();
}

void loop()
{
	Main_Mode_Run();
}


////////////////////////
// Main_Mode	     //
//////////////////////
void Main_Mode_Setup()
{
	Serial.begin(115200);
	Wifi_Setup();
	LCD_Setup();
	BUTTON_Setup();
	RFID_Setup();
}

//Initializing Main Mode variable
void Main_Mode_Init()
{
	for(int i = 0; i < 9 ; i++)
	{
		RFID_idValue[i] = NULL;
	}
	RFID_id = NULL;
}

//Main_Mode_Runnable
void Main_Mode_Run()
{
	char Main_status = 0;
	char Button, LCD, Wifi, RFID, ButtonInput=0;

	Main_Mode_Init();
	Button = BUTTON_Init();
	LCD = LCD_Init();
	Wifi = Wifi_Init();
	RFID = RFID_Init();

	ButtonInput = BUTTON_Routine();

	Main_Routine ( ButtonInput , Main_Mode( Button, LCD, Wifi, RFID ) );
}

//Check every device alive
char Main_Mode( char Button, char LCD, char Wifi, char RFID )
{
	if( (Button == ACTIVE) && (LCD == ACTIVE) && (Wifi == ACTIVE) && (RFID == ACTIVE) )
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void Main_Routine ( char Button_Input, char Main_Status)
{
	if (Main_Status == ACTIVE)
	{
		if(Button_Input == IN_PROCESSING_MODE )
		{
			Main_InProcessingMode();
			//Debug
			//Serial.println(F("InProc"));
		}
		else if(Button_Input == OUT_PROCESSING_MODE)
		{
			Main_OutProcessingMode();
			//Serial.println(F("OutProc"));
		}

		else if(Button_Input == CHECK_PROCESSING_MODE)
		{
			Main_CheckProcessingMode();
			//Serial.println(F("Main_CheckProc"));
		}

		else
		{
			Main_WaitingMode();
			//Serial.println(F("WaitingMode"));
		}
	}
	else if (Main_Status == INACTIVE)
	{
		Main_ErrorProcessingMode();
		//Serial.println(F("Error"));
	}
	else
	{

	}
}

//In Processing Mode
void Main_InProcessingMode()
{
	int ID_Status=0;
	char Name[20];
	char* Packet;

	for (int i = 0; i < 8 ; i++)
	{
		RFID_idValue[i] = *RFID_id;
		RFID_id++;
	}

	//Personal ID was written in DB successfully without error
	LCD_Print_L2("IN-PROCESSING","PLAEASE TAG!");
	RFID_id = RFID_Receive(RFID_WAITING_TIME);
	Serial.println(RFID_id);
	//	Packet = Wifi_MakePacket(RFID_id, 0);
	//	Serial.print("Packet : ");
	//	Serial.println(Packet);
	//ID_Status = Wifi_sendPacket(Packet);
	ID_Status = Wifi_sendPacket_Test(RFID_id);
	//ID_Status = Wifi_SendID(IN_MODE, Rfid_id, WIFI_MAX_TRIAL);
	//Name = Wifi_GetName(Rfid_id);
	//ID_Status = WRITE_SUCCESSFUL;

	if(RFID_id != NULL)
	{
		//Tag was successfully inserted
		if( ID_Status == WRITE_SUCCESSFUL )
		{
			LCD_Print_L2(RFID_id, "HAVE A GOOD DAY");
			BUZZER_Beep( BUZ_1_PIN, BEEP_VOLUME, BEEP_SOUND_MODE_INP6 );
		}

		//Current person ID was already tagged
		else if( ID_Status == DUPLICATE )
		{
			LCD_Print_L2(RFID_id, "ALREADY TAGGED");
			BUZZER_Beep( BUZ_1_PIN, BEEP_VOLUME, BEEP_SOUND_MODE_INP2 );
		}

		//Current personnel ID was not in the DB
		else if( ID_Status == NOT_IN_DB )
		{
			LCD_Print_L2(RFID_id, "PLEASE REGISTER");
			BUZZER_Beep( BUZ_1_PIN , BEEP_VOLUME, BEEP_SOUND_MODE_INP2 );
		}

		//Unknown case
		else
		{
			LCD_Print_L1("UNKNOWN ERROR");
			BUZZER_Beep( BUZ_1_PIN, BEEP_VOLUME, BEEP_SOUND_MODE_INP6 );
		}
	}
	else
	{
		LCD_Print_L2("NO CARD DETECTED", "TRY AGAIN");
		BUZZER_Beep( BUZ_1_PIN, BEEP_VOLUME, BEEP_SOUND_MODE_INP5);
	}
}

//Out Processing Mode
void Main_OutProcessingMode()
{
	int ID_Status=0;

	for (int i = 0; i < 8 ; i++)
	{
		RFID_idValue[i] = *RFID_id;
		RFID_id++;
		//Serial.print(RFID_idValue[i]);
	}
	/*
	for (int j = 0; j < 8; j++)
	{
		RFID_id--;
	}
	*/
	//Serial.print(RFID_id);
	//Serial.print("/");

	//Personal ID was written in DB successfully without error
	LCD_Print_L2("OUT-PROCESSING","PLAEASE TAG!");
	RFID_id = RFID_Receive(RFID_WAITING_TIME);
	//ID_Status = Wifi_SendID(Send_Mode, Rfid_id, WIFI_MAX_TRIAL);
	ID_Status = WRITE_SUCCESSFUL;

	if(RFID_id != NULL)
	{
		//Tag was successfully inserted
		if( ID_Status == WRITE_SUCCESSFUL )
		{
			LCD_Print_L2(RFID_id, "THANK YOU!");
			BUZZER_Beep( BUZ_1_PIN, BEEP_VOLUME, BEEP_SOUND_MODE_INP6 );
		}

		//Current person ID was already tagged
		else if( ID_Status == DUPLICATE )
		{
			LCD_Print_L2(RFID_id, "ALREADY TAGGED");
			BUZZER_Beep( BUZ_1_PIN, BEEP_VOLUME, BEEP_SOUND_MODE_INP2 );
		}

		//Current personnel ID was not in the DB
		else if( ID_Status == NOT_IN_DB )
		{
			LCD_Print_L2(RFID_id, "PLEASE REGISTER");
			BUZZER_Beep( BUZ_1_PIN , BEEP_VOLUME, BEEP_SOUND_MODE_INP2 );
		}

		//Unknown case
		else
		{
			LCD_Print_L1("UNKNOWN ERROR");
			BUZZER_Beep( BUZ_1_PIN, BEEP_VOLUME, BEEP_SOUND_MODE_INP6 );
		}
	}
	else
	{
		LCD_Print_L2("NO CARD DETECTED", "TRY AGAIN");
		BUZZER_Beep( BUZ_1_PIN, BEEP_VOLUME, BEEP_SOUND_MODE_INP5);
	}
}

//Check Processing Mode
void Main_CheckProcessingMode()
{
	LCD_Print_L2("MAIN_CHECK PROC", "PLEASE TAG CARD");
	//delay(100);
}

//Error Processing Mode
void Main_ErrorProcessingMode()
{
	LCD_Print_L1("ERROR PROCESSING");
	//delay(100);
}

void Main_WaitingMode()
{
	LCD_Print_L2("CHANG-SIN", "PRECISION");
}

/////////////////////
//BUTTON LIBRARY  //
////////////////////

bool BUTTON_Setup()
{
	pinMode(BUTTON1_NO, 0);
	pinMode(BUTTON2_NO, 0);
	pinMode(BUTTON3_NO, 0);
	return 1;
}

char BUTTON_Init()
{
	BUTTON_Setup();
	return 1;
}

char BUTTON_Routine()
{
	BUTTON_Setup();
	int Inp1=0, Inp2=0, Inp3=0;
	char CheckMode=0;

	Inp1 = BUTTON_GetInput(BUTTON1_NO);
	Inp2 = BUTTON_GetInput(BUTTON2_NO);
	Inp3 = BUTTON_GetInput(BUTTON3_NO);
	CheckMode = Inp1 + Inp2 + Inp3 ; //Check how many buttons are pressed
	//Serial.print(Inp1);
	//Serial.print(Inp2);
	//Serial.println(Inp3);

	if(CheckMode == 3) // No button is pressed
	{
		return 0;
	}
	else if (CheckMode == 2) // Only one button is pressed
	{
		if( Inp1 == 0 )
		{
			return 1;
		}
		else if( Inp2 == 0 )
		{
			return 2;
		}
		else if( Inp3 == 0 )
		{
			return 3;
		}
		else
		{
			return 0;
		}
	}
	else if ( CheckMode == 1 || CheckMode == 0 ) // Two buttons are presssed --> failure mode
	{
		return 10;
	}
	else
	{
		return 0;
	}
}

int BUTTON_GetInput(int ButtonNumber)
{
	pinMode(ButtonNumber, 0);
	return digitalRead(ButtonNumber);
}

bool BUZZER_Setup()
{
	pinMode(BUZ_1_PIN, OUTPUT);
	return true;
}

void BUZZER_Beep(int BuzzerPin, int Volume, int MelodyType)
{
	pinMode(BuzzerPin, 1);

	if(MelodyType == 0) //Short Beep once 200ms
	{
		tone(BuzzerPin, 1000, 200);
	}

	else if(MelodyType == 1) // Long Beep once 1s
	{
		tone(BuzzerPin, 1000, 1000);
	}

	else if(MelodyType == 2) // Same tone with twice 200ms duration
	{
		tone(BuzzerPin, 1000, 100);
		delay(100);
		tone(BuzzerPin, 1000, 100);
	}

	else if(MelodyType == 3) // Same tone with twice 1s duration
	{
		tone(BuzzerPin, 1000, 1000);
		delay(100);
		tone(BuzzerPin, 1000, 1000);
	}

	else if(MelodyType == 4) // Different tone going up 200ms duration
	{
		tone(BuzzerPin, 500, 200);
		delay(100);
		tone(BuzzerPin, 1000, 200);
	}

	else if(MelodyType == 5 ) // Different tone going down 200ms duration
	{
		tone(BuzzerPin, 1000, 200);
		delay(100);
		tone(BuzzerPin, 500, 200);
	}

	else if(MelodyType == 6 ) // Do-Mi-Sol-Do 200ms duration
	{
		tone(BuzzerPin, 500, 100);
		delay(100);
		tone(BuzzerPin, 1000, 100);
		delay(100);
		tone(BuzzerPin, 1500, 100);
		delay(100);
		tone(BuzzerPin, 2000, 100);
	}
}

void BUZZER_Test()
{
	pinMode(BUZ_1_PIN, 1);
    tone(BUZ_1_PIN, 1000, 200);
    delay(100);
    tone(BUZ_1_PIN, 500, 200);
}


//LCD Library

void LCD_Setup()
{
	LCD_0.setFont(u8g_font_unifont);
	LCD_0.setColorIndex(1);
	LCD_0.firstPage();
}

char LCD_Init()
{
	//LCD_Setup();
	return 1;
}

void LCD_Print_L1(char* stringStream)
{
	LCD_PrintLine1(LCD_0, LCD_LINE1_X, LCD_LINE1_Y, stringStream);
}

void LCD_Print_L2(char* stringStream, char* stringStream2)
{
	LCD_PrintLine2(LCD_0, LCD_LINE1_X, LCD_LINE1_Y, stringStream, LCD_LINE2_X, LCD_LINE2_Y, stringStream2);
}

//LCD Character Printing Librarys
//Print Character string 1 line
void LCD_PrintLine1(U8GLIB_SH1106_128X64_2X dev, uint8_t posX1, uint8_t posY1, char* stringStream1)
{
  dev.firstPage();
  do {
    dev.drawStr( posX1, posY1, stringStream1);
  } while( dev.nextPage() );
}

//Print Character string 2 lines
void LCD_PrintLine2(U8GLIB_SH1106_128X64_2X dev, uint8_t posX1, uint8_t posY1, char* stringStream1, uint8_t posX2, uint8_t posY2, char* stringStream2)
{
  dev.firstPage();
  do {
    dev.drawStr( posX1, posY1, stringStream1);
    dev.drawStr( posX2, posY2, stringStream2);
  } while( dev.nextPage() );
}

	///////////////////////
	//RFID Library		 //
	///////////////////////

void RFID_Setup()
{

}

char RFID_Init()
{
	//RFID Initializaiton
	for (int i = 0; i < 4; i++ )
	{
		nuidPICC[i] = NULL;
	}
	for (int j = 0; j < 8; j++ )
	{
		CardUid[j] = NULL;
	}
	return 1;
}

void printHex(byte *buffer, byte bufferSize)
{
  for (byte i = 0; i < bufferSize; i++)
  {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}

/**
 * Helper routine to dump a byte array as dec values to Serial.
 */
void printDec(byte *buffer, byte bufferSize)
{
  for (byte i = 0; i < bufferSize; i++)
  {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], DEC);
  }
}

bool RFID_ReadID()
{
	rfid.PCD_Init(); // Init MFRC522
	rfid.PCD_SetAntennaGain(111);

	// Look for new cards
	if ( ! rfid.PICC_IsNewCardPresent())
	return false;
	// Verify if the NUID has been readed
	if ( ! rfid.PICC_ReadCardSerial())
	return false;

	if (rfid.uid.uidByte[0] != nuidPICC[0] ||
	rfid.uid.uidByte[1] != nuidPICC[1] ||
	rfid.uid.uidByte[2] != nuidPICC[2] ||
	rfid.uid.uidByte[3] != nuidPICC[3] )
	{
		Serial.println(F("A new card has been detected."));

		// Store NUID into nuidPICC array
		for (byte i = 0; i < 4; i++)
		{
		nuidPICC[i] = rfid.uid.uidByte[i];
		}

		if( (rfid.uid.uidByte[0] * rfid.uid.uidByte[1] * rfid.uid.uidByte[2] * rfid.uid.uidByte[3] ) != 0 )
		{
		//Convert byte Array to Character Array
		for (int i = 0; i < 4; i++)
		{
			int CharIndex = i * 2;
			char buf0=0, buf1=0;
			buf0 = ( rfid.uid.uidByte[i] / 16 ) ;
			buf1 = ( rfid.uid.uidByte[i] - buf0 * 16 ) ;

			if(buf0 <  10)
			{
				CardUid[CharIndex] = buf0 + 48 ;
				Serial.print("Debug");
				Serial.print(CardUid[CharIndex]);
			}
			else if( buf0 < 16 )
			{
				Serial.print("Debug");
				CardUid[CharIndex] = buf0 + 55 ; // 48 + 7
				Serial.print(CardUid[CharIndex]);
			}

			if(buf1 <  10)
			{
				CardUid[(CharIndex+1)] = buf1 + 48 ;
				Serial.print("Debug");
				Serial.print(CardUid[(CharIndex+1)]);
			}
			else if( buf1 < 16 )
			{
				Serial.print("Debug");
				CardUid[(CharIndex+1)] = buf1 + 55;
				Serial.print(CardUid[(CharIndex+1)]);
			}
		}
	}

    Serial.println(F("The NUID tag is:"));
    Serial.print(F("In hex: "));
    printHex(rfid.uid.uidByte, rfid.uid.size);
    Serial.println();
    Serial.print(F("In dec: "));
    printDec(rfid.uid.uidByte, rfid.uid.size);
    Serial.println();
    }

  rfid.PCD_StopCrypto1();
  return true;
}

char* RFID_Receive(unsigned int WaitingTimeMS)
{
	bool check;

	for ( int i = 0; i < WaitingTimeMS ; i++ )
	{
		check = RFID_ReadID();
		Serial.print(i);
		if( check != false )
		{
			return CardUid;
		}
		else
		{
			return NULL;
		}
	}
	return NULL;
}



void Wifi_Setup()
{
	//esp8266Serial.begin(115200);
	//esp8266Serial.setTimeout(5000);
	Wifi_ConnectInit("iptime", "o533540869", "192.168.0.14","27015");
	//Wifi_ConnectInit("Trojan_Horse", WIFI_AP_PASSWORD, WIFI_SERVER_IP, WIFI_SERVER_PORT);

	//Test ESP8266 Status
	/*
	do
	{
		esp8266Serial.println("AT");
		//Debug purpose
		Serial.println(F("AT"));
		if( i >= WIFI_MAX_TRIAL )
		{
			//return 1;
		}
		i++;
		if ( (esp8266Serial.available() > 0 ) && ( esp8266Serial.find("OK") == 1 ) )
		{
			Serial.println(F("ESP8266_LIVE"));
			break;
		}
	} while( 1 );

	//AP MODE SETTING
	/*
	i = 0;
	do
	{
		esp8266Serial.println(F("AT+CWMODE=3"));
		//Debug purpose
		Serial.println(F("AT+CWMODE=3"));
		if( i >= WIFI_MAX_TRIAL )
		{
			return 2;
		}
		i++;
		if ( (esp8266Serial.available() > 0 ) && ( esp8266Serial.find("OK") == 1 ) )
		{
			Serial.println(F("AT+CWMODE=3, SUCCESS"));
			break;
		}
	} while( 1 );

	//AP CONNECTION
	i = 0;
	do
	{
		esp8266Serial.println(F("AT+CWJAP=\"Trojan_Horse\",\"qwertyui\""));

		//Debug purpose
		Serial.println(F("AT+CWJAP=\"Trojan_Horse\",\"qwertyui\""));

		if( i >= WIFI_MAX_TRIAL )
		{
			return 3;
		}
		i++;

		if( (esp8266Serial.available()>0 ) && (esp8266Serial.find("OK") == 1) )
		{
			Serial.println(F("AP Connected"));
		}
	} while( 1 );

	//SET TCP CONNECTION
	i = 0;
	do
	{
		esp8266Serial.println("AT+CIPSTART=\"TCP\",\"192.168.0.28\",27015");

		//Debug purpose
		Serial.println(F("AT+CIPSTART=\"TCP\",\"192.168.0.28\",27015"));
		if( i >= WIFI_MAX_TRIAL )
		{
			return 4;
		}
		i++;
		if( (esp8266Serial.available()>0 ) && (esp8266Serial.find("OK") == 1) )
		{
			Serial.println(F("TCP_Connected"));
		}
	} while( 1 );
	//Wifi_ConnectInit(WIFI_AP_NAME, WIFI_AP_PASSWORD);
	 */
}

bool Wifi_Alive_Check()
{
	esp8266Serial.begin(115200);
	for(int i=0; i < WIFI_MAX_TRIAL; i++)
	{
		esp8266Serial.println(F("AT"));
		if ( (esp8266Serial.available() > 0 ) && ( esp8266Serial.find("OK") == 1 ) )
		{
			Serial.println(F("ESP8266_LIVE!"));
			esp8266Serial.end();
			return true;
		}
		else
		{
			Serial.println(F("Not Alive"));
		}
	}
	esp8266Serial.end();
	return false;
}

//Check Wifi Status and return TRUE when Wifi connection is established
bool Wifi_Init()
{

	//ConnectionStatus = Wifi_ConnectionCheck();
	char ConnectionStatus = 1;

	for (int i=0; i<13; i++)
	{
		Wifi_Packet[i]='\0';
	}

	if(ConnectionStatus == WIFI_CONNECTED)
	{
		ConnectionStatus = 1;
		return 1;
	}

	else if(ConnectionStatus == WIFI_DISCONNECTED)
	{
		if( Wifi_ConnectAP(WIFI_AP_NAME, WIFI_AP_PASSWORD) == 1 )
		{
			ConnectionStatus = 1;
			return 1;
		}
		ConnectionStatus = 0;
		return 0;
	}

	else
	{
		// Do nothing
	}
}

bool Wifi_ConnectInit(String APName_, String APPassword_, String SVRIP_, String SVRPort_)
{
	bool Wifi_Alive_flag=false, Wifi_Mode_flag=false, Wifi_ConnectAP_flag=false, Wifi_ConnectTCP_flag=false;

	Wifi_Alive_flag = Wifi_Alive_Check();

	esp8266Serial.begin(115200);
	esp8266Serial.setTimeout(4000);
	for(int i=0; i < WIFI_MAX_TRIAL; i++)
	{
		esp8266Serial.println("AT+CWMODE=3");
		if ( (esp8266Serial.available() > 0 ) &&
				( ( esp8266Serial.find("OK") || esp8266Serial.find("change=3") ) == 1 ) )
		{
			esp8266Serial.end();
			Serial.println(F("CWMODE_SET"));
		}
		else
		{
			//Serial.println()
			Serial.println(F("CWMODE_SETFAIL"));
		}

	}
	Wifi_ConnectAP_flag = Wifi_ConnectAP(APName_, APPassword_);
	Wifi_ConnectTCP_flag = Wifi_ConnectTCP(SVRIP_, SVRPort_);

	if( (Wifi_Alive_flag == 1 && Wifi_ConnectAP_flag == 1 && Wifi_ConnectTCP_flag == 1 ) )
	{
		return true;
	}
	else
	{
		return false;
	}
}

//
bool Wifi_ConnectAP(String APName, String APPassword)
{
	esp8266Serial.begin(115200);
	esp8266Serial.setTimeout(4000);
	String command = "AT+CWJAP=\""+APName+"\",\""+APPassword+"\"";
	Serial.println(command);
	for(int i=0; i < WIFI_MAX_TRIAL; i++)
	{
		esp8266Serial.println(command);
		if ( (esp8266Serial.available() > 0 ) && ( esp8266Serial.find("OK") == 1 ) )
		{
			Serial.println(F("AP Connected!"));
			esp8266Serial.end();
			return true;
		}
		else
		{
			Serial.println(F("AP Connect Fail"));
		}
	}
	esp8266Serial.end();
	return false;
}

//String ServerIP, String Port
bool Wifi_ConnectTCP(String SVRIP, String SVRPort)
{
	esp8266Serial.begin(115200);
	esp8266Serial.setTimeout(5000);
	String ServerIPCommand = "AT+CIPSTART=\"TCP\",\""+SVRIP+"\","+SVRPort;
	Serial.println(ServerIPCommand);
	for(int i=0; i < WIFI_MAX_TRIAL; i++)
	{

		esp8266Serial.println(ServerIPCommand);
		if ( (esp8266Serial.available() > 0 ) && ( esp8266Serial.find("OK") == 1 ) )
		{
			Serial.println(F("TCP Connected!"));
			esp8266Serial.end();
			return true;
		}
		else
		{
			Serial.println(F("TCP Not connected"));
		}
	}
	esp8266Serial.end();
	return false;
}

char Wifi_ConnectionCheck()
{
	char check[5] = {'c','h','e','c','k'};
	char Echo[2];

	Wifi_receivePacket(Echo);

	if(Echo[0] == 'o' && Echo[1] =='k')
	{
		return WIFI_CONNECTED;
	}
	else
	{
		return WIFI_DISCONNECTED;
	}
}

//
int Wifi_sendPacket(char* Input)
{
	//String PushPacket="ABCD12345678";
	String PushPacket = "0", PushPacket2;
	char PushPacket3[12];

	for(int i=0; i < WIFI_MAX_TRIAL; i++)
	{
		esp8266Serial.println(F("AT+CIPSEND=13"));
		esp8266Serial.println(Input);
		Serial.print("Read String : ");
		Serial.println(esp8266Serial.readString());
		if ( (esp8266Serial.available() > 0 ) ) //&& ( esp8266Serial.find("SEND OK") == 1 ) )
		{
			Serial.print("ESP Received String : ");
			Serial.println(esp8266Serial.readString());

			Serial.println(F("Send Success!"));
			return true;
		}
		else
		{
			Serial.println(F("Send Fail!"));
		}
	}
	return false;
}

int Wifi_sendPacket_Test(char* Input)
{
	//String PushPacket="ABCD12345678";
	String PushPacket = "0", PushPacket2;
	char PushPacket3[9];

	for(int i=0; i < WIFI_MAX_TRIAL; i++)
	{
		esp8266Serial.println(F("AT+CIPSEND=9"));
		esp8266Serial.println(Input);
		//Serial.print("Read String : ");
		Serial.println(esp8266Serial.readString());
		if ( (esp8266Serial.available() > 0 ) ) //&& ( esp8266Serial.find("SEND OK") == 1 ) )
		{
			Serial.println(F("Send Success!"));
			return true;
		}
		else
		{
			Serial.println(F("Send Fail!"));
		}
	}
	return false;
}


void Wifi_receivePacket(char* Echo)
{

}

char* Wifi_MakePacket(char* InputUUID, int Mode)
{
	Wifi_Packet[0]='1';
	Wifi_Packet[1]= Mode + '0';

	for (int i = 2; i < 10; i++)
	{
		Wifi_Packet[i] = *InputUUID;
		InputUUID++;
	}

	// Checksum Disable
	Wifi_Packet[10] = 'E';
	Wifi_Packet[11] = 'E';

	Serial.print("ResultPacket : ");
	Serial.println(Wifi_Packet);
	return Wifi_Packet;
}
